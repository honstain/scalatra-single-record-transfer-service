package org.bitbucket.honstain.inventory.app

import org.bitbucket.honstain.inventory.dao._
import org.scalatra._
import org.slf4j.{Logger, LoggerFactory}

import scala.util.{Failure, Success}
// JSON-related libraries
import org.json4s.{DefaultFormats, Formats}
// JSON handling support from Scalatra
import org.scalatra.json._

import slick.jdbc.PostgresProfile

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits.global

class ToyInventory(database: PostgresProfile.backend.DatabaseDef) extends ScalatraServlet with JacksonJsonSupport {

  val random: scala.util.Random = scala.util.Random
  val logger: Logger = LoggerFactory.getLogger(getClass)
  protected implicit val jsonFormats: Formats = DefaultFormats

  before() {
    contentType = formats("json")
  }

  get("/") {
    val futureResult = Await.result(InventorySingleRecordDao.findAll(database), Duration.Inf)
    logger.debug(s"GET retrieved ${futureResult.size} inventory records")
    Ok(futureResult.map(x => Inventory(x.sku, x.qty, x.location)))
  }

  post("/") {
    val newInventory = parsedBody.extract[Inventory]
    logger.debug(s"Creating inventory sku:${newInventory.sku} in location:${newInventory.location}")

    val future: Future[Option[InventorySingleRecord]] = InventorySingleRecordDao.create(database, newInventory.sku, newInventory.qty, newInventory.location)
    future.onComplete {
      case Success(Some(value)) => logger.debug(s"Created new inventory record id:${value.id}")
      case Success(None) =>
        // TODO - Does it even make sense to try and account for a case like this?
        logger.error(s"Failed to create or update sku:${newInventory.sku} in location:${newInventory.location}")
      case Failure(t) => logger.error(t.getMessage)
    }
    val result = Await.result(future, Duration.Inf)
    if (result.isDefined) {
      logger.debug(s"Created new inventory record id:${result.get}")
      // TODO - decide on more appropriate response data
      Ok(result.map(x => Inventory(x.sku, x.qty, x.location)))
    }
    else {
      InternalServerError
    }
  }

  post("/transfer") {
    val transfer = parsedBody.extract[InventoryTransfer]
    val fakeUserId = random.nextInt(1000).toString
    logger.debug(s"user: $fakeUserId Transfer sku:${transfer.sku} from:${transfer.fromLocation} to:${transfer.toLocation}")

    val future: Future[Int] = InventorySingleRecordDao.transfer(database, transfer.sku, transfer.qty, transfer.fromLocation, transfer.toLocation, fakeUserId)
    future.onComplete {
      case Success(t) => logger.debug(s"user: $fakeUserId Transfer transaction success $t")
      case Failure(t) => logger.debug(s"user: $fakeUserId ${t.getMessage}")
    }
    Await.result(future, Duration.Inf)
    Ok()
  }
}

case class Inventory(sku: String, qty: Int, location: String)
case class InventoryTransfer(sku: String, qty: Int, fromLocation: String, toLocation: String)