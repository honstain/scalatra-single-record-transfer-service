package org.bitbucket.honstain.inventory.app

import org.bitbucket.honstain.PostgresSpec
import org.json4s.DefaultFormats
import org.scalatra.test.scalatest._
import org.json4s.jackson.Serialization.write
import org.scalatest.BeforeAndAfter
import slick.dbio.DBIO
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Await
import scala.concurrent.duration.Duration


class ToyInventoryTests extends ScalatraFunSuite with BeforeAndAfter with PostgresSpec {

  implicit val formats = DefaultFormats

  addServlet(new ToyInventory(database), "/*")

  def createInventoryTable: DBIO[Int] =
    sqlu"""
        CREATE TABLE inventory_single
        (
          id bigserial NOT NULL,
          sku text,
          qty integer,
          location text,
          CONSTRAINT pk_single PRIMARY KEY (id),
          UNIQUE (sku, location)
        );
      """
  def dropInventoryTable: DBIO[Int] =
    sqlu"""
          DROP TABLE IF EXISTS inventory_single;
      """

  before {
    Await.result(database.run(createInventoryTable), Duration.Inf)
  }

  after {
    Await.result(database.run(dropInventoryTable), Duration.Inf)
  }

  val TEST_SKU = "NewSku"
  val BIN_01 = "Bin-01"
  val BIN_02 = "Bin-02"

  /** *********************************************************************************
    * The tests endpoints that use the single entry accounting database design
    * *********************************************************************************
    */
  test("GET / for no inventory") {
    get("/") {
      status should equal (200)
      body should equal (write(List()))
    }
  }

  test("GET / with a single item of inventory") {
    post("/", write(Inventory(TEST_SKU, 3, BIN_01))) {}
    get("/") {
      status should equal (200)
      body should equal (write(List(Inventory(TEST_SKU, 3, BIN_01))))
    }
  }

  test("POST / to create inventory") {
    post("/", write(Inventory(TEST_SKU, 3, BIN_01))) {
      status should equal (200)
    }
  }

  test("POST //transfer to move inventory") {
    post("/", write(Inventory(TEST_SKU, 3, BIN_01))) {}
    post("/", write(Inventory(TEST_SKU, 1, BIN_02))) {}
    post("/transfer", write(InventoryTransfer(TEST_SKU, 3, BIN_01, BIN_02))) {
      status should equal (200)
      get("/") {
        status should equal (200)
        body should equal (write(List(
          Inventory(TEST_SKU, 4, BIN_02),
          Inventory(TEST_SKU, 0, BIN_01),
        )))
      }
    }
  }
}